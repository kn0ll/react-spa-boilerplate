# react-spa-boilerplate
a pretty opinionated boilerplate. popular opinion though.

---

## contains

### application core
- react
- redux
- immutable
- scss modules

### develop & build
- babel (react, es2015, stage-0, decorators)
- react hot loading
- css modules (hot reloaded and inlined in dev, but compiles to a single external stylesheet for dist)
- webpack dev server configuration & dashboard
- webpack build & distribution configuration
- static file server for testing built applications

---

## instructions

### install dependencies
```
npm install
```
this will install all application requisites to develop and run the application.

### run a development server
```
npm run-script serve:development
```
this will start a hot reloading development server at [http://localhost:8080](http://localhost:8080).

### build & package
```
npm run-script build:production
```
this will package all assets and create a static application in `dist/`.

### serve packaged build
```
npm run-script serve:production
```
this will start a static server hosting the build application in `dist/` at [http://localhost:8080](http://localhost:8080).