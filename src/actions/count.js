import { createAction } from 'redux-actions';

export const setCount = createAction('SET_COUNT');