import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { setCount } from '../actions/count';

import Counter from './Counter';

import styles from './app.scss';

const mapStateToProps = (state) => {
  return {
    count: state.get('count')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCount: (count) => {
      dispatch(setCount(count));
    }
  };
};

@connect(mapStateToProps, mapDispatchToProps)
class App extends PureComponent {

  static propTypes = {
    count: React.PropTypes.number.isRequired,
    setCount: React.PropTypes.func.isRequired
  };

  render() {
    const { count, setCount } = this.props;

    return (
      <div className={styles['app']}>
        <h1>App</h1>
        <Counter
          count={count}
          setCount={setCount} />
      </div>
    );
  }

}

export default App;
