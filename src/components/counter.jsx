import React, { PureComponent } from 'react';

import styles from './counter.scss';

class Counter extends PureComponent {

  static propTypes = {
    count: React.PropTypes.number.isRequired,
    setCount: React.PropTypes.func.isRequired
  };

  componentDidMount() {
    this.interval = setInterval(this.tick.bind(this), 1000);
  }

  tick() {
    const { count, setCount } = this.props;

    setCount(count + 1);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { count } = this.props;

    return (
      <div className={styles['counter']}>
        Count: <span className={styles['count']}>{count}</span>
      </div>
   );
  }

}

export default Counter;