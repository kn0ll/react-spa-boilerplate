import { handleActions } from 'redux-actions';

import { setCount } from '../actions/count';

import initialState from '../state';

let countReducer = handleActions({

  [setCount]: (state, action) => {
    return action.payload;
  }

}, initialState.get('count'));

export default countReducer;