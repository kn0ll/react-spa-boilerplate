import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  count: 0
});

export default initialState;