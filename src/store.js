import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux-immutable';
import createLogger from 'redux-logger';

import initialState from './state';

import countReducer from './reducers/count';

const composeEnhancers = process.env.NODE_ENV != 'production'?
  (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose):
  compose;

const reducer = combineReducers({
  count: countReducer
});

const middleware = [];

if (process.env.NODE_ENV != 'production') {
  middleware.push(createLogger({ collapsed: true, timestamp: false }));
}

const store = createStore(reducer, initialState, composeEnhancers(
  applyMiddleware(...middleware)
));

export default store;